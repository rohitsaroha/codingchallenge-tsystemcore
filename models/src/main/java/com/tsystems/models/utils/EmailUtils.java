package com.tsystems.models.utils;


import org.apache.commons.beanutils.BeanUtils;

import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.util.Map;

/**
 * Generic Email Util class to do simple conversion function to be used during various application flows.
 */
public class EmailUtils {
// Converts the mime message into string and then can be used for logging to uploading the document in document store for keeping record of client communications
    public static String mimeMessageToString(MimeMessage message) {
        try(ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            message.writeTo(os);
            return os.toString();
        } catch (Exception var4) {
            throw new RuntimeException("Error in coverting email message to string: "+var4.getMessage());
        }

    }
    // return a map of notificationMessage properties which will differ based on type of communication channel
    public static Map<String,String> getNotificationProperties(Object bean){
        Map<String, String> senderAndReceiverDetails=null;
        try {
            senderAndReceiverDetails=BeanUtils.describe(bean);
        }
        catch(Exception e){
            throw new RuntimeException("Exception in preparing object properties map: "+e.getMessage());
        }
        return senderAndReceiverDetails;
    }
}
