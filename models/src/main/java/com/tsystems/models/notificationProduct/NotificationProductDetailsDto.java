package com.tsystems.models.notificationProduct;

/**
 * The Data transfer object of notification Product to be used for exchanging info among different services.
 * In no case the entity Object should be exposed to other services for data exchange
 */
public class NotificationProductDetailsDto {
    private String notificationProductId;
    private String notificationProductName;
    // email , sms or push notification
    private String notificationType;
    private String usedQuota;
    private boolean isActive;
}
