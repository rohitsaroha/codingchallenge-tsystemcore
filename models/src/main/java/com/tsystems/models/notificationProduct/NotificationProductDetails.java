package com.tsystems.models.notificationProduct;

import java.util.Date;
/** The JPA entity class to store the type of product and their details
The use case arises when for certain type of channels we employs more than one service provider
 For Example - for sending email more than 50K use company A service and less than that use Company B email service
**/
public class NotificationProductDetails {
    private long id;
    private String notificationProductId;
    private String notificationProductName;
    // email , sms or push notification
    private String notificationType;
    private boolean isActive;
    private Date notificationProductStartDate;
    private Date notificationProductEndDate;
}
