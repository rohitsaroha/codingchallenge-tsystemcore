package com.tsystems.models.converters;

import com.tsystems.models.clientDetails.ClientDetailsDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
// This is a map construct implementation which can be used
// for conversion of data objects from one to another efficiently.
@Mapper
public interface ClientDetailsConverter {
    ClientDetailsConverter INSTANCE= Mappers.getMapper(ClientDetailsConverter.class);
    /**
    @Mapping(source="transactionId", target="filename")
    Client convertToClient(ClientDetailsDto clientDetailsDto);

    @Mapping(source="filename", target="transactionId")
    ClientDetailsDto convertToClientDto(Client client);
**/
}
