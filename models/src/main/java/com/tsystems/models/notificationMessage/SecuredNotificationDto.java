package com.tsystems.models.notificationMessage;

import com.tsystems.models.authentication.AuthDto;

import javax.validation.constraints.NotBlank;

/**
 * The Wrapper notification containing the security notificaition and notification message itself
 * It will be sent by lambda function on queue and it will be consumed one of the listners of notificaiton service.
 */
public class SecuredNotificationDto {
    // Security details of the subscribed client
    @NotBlank
    private AuthDto authDto;
    // message that needs to be sent out to end user
    @NotBlank
    private INotificationMessage notificationMessageDto;

    public AuthDto getAuthDto() {
        return authDto;
    }

    public void setAuthDto(AuthDto authDto) {
        this.authDto = authDto;
    }

    public INotificationMessage getNotificationMessageDto() {
        return notificationMessageDto;
    }

    public void setNotificationMessageDto(INotificationMessage INotificationMessageDto) {
        this.notificationMessageDto = INotificationMessageDto;
    }
}
