package com.tsystems.models.notificationMessage;

import org.apache.commons.beanutils.BeanUtils;

import java.util.Map;

// Generic interface for standardization of notification messages and altering application flows on run time
public interface INotificationMessage {
// returns notification type to be used for changing the application flows accordingly on run time.
NotificationType getNotificationType();
// the notification message is core of the service so every channel needs to implement this method , so that services can use it accordingly.
String getNotificationMessage();
// important to keep track of messages sent by subscribed client
String getMessageId();
//  provides the status of notification message after processing
NotificationProcessingResult getNotificationProcessingStatus();
// provides the processing notes if any added by service layers
String getNotificationProcessingNotes();
}
