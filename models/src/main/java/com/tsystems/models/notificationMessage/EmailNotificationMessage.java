package com.tsystems.models.notificationMessage;

import com.tsystems.models.utils.EmailUtils;

import javax.mail.internet.MimeMessage;
import javax.validation.constraints.NotBlank;

/*
Email Message which will sent by notification service to end user

 */
public class EmailNotificationMessage implements INotificationMessage {

    @NotBlank
    private String senderAddress;
    @NotBlank
    private String receiverAddress;
    @NotBlank
    private MimeMessage emailMessage;
    @NotBlank
    private String messageId;
    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public MimeMessage getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(MimeMessage emailMessage) {
        this.emailMessage = emailMessage;
    }

    @Override
    public NotificationType getNotificationType() {
        return NotificationType.EMAIl;
    }

    @Override
    public String getNotificationMessage() {
        return EmailUtils.mimeMessageToString(emailMessage);
    }

    @Override
    public String getMessageId() {
        return messageId;
    }

    @Override
    public NotificationProcessingResult getNotificationProcessingStatus() {
        return null;
    }

    @Override
    public String getNotificationProcessingNotes() {
        return null;
    }
}
