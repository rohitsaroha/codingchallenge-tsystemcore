package com.tsystems.models.notificationMessage;

public enum NotificationProcessingResult {
    SUCCESS,SKIPPED,FAILURE
}
