package com.tsystems.models.clientDetails;

import com.tsystems.models.subscriptionDetails.SubscriptionDetailsDto;

import java.util.List;
import java.util.Map;

// The data transfer object containing subscribed details info which will be sent to us by client details microservice
public class ClientDetailsDto {
    private long clientId;
    private String clientName;
    private String subscriptionId;
    private List<SubscriptionDetailsDto> subscriptionDetailsDtos;
    private boolean isActive;
}
