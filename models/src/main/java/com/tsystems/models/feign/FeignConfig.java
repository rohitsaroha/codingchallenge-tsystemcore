package com.tsystems.models.feign;

import lombok.Data;
import lombok.NoArgsConstructor;

/*
The Class holds general configuration to be used while creating feign connection
The usage of this class can be seen in notification service App config, where the application .properties file
are used to fill up below conenction parameters.
 */
public class FeignConfig {
    private String feignUrl;
    private String userName;
    private String password;
    private int feignConnectTimeout;
    private int feignReadTimeout;

    public String getFeignUrl() {
        return feignUrl;
    }

    public void setFeignUrl(String feignUrl) {
        this.feignUrl = feignUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getFeignConnectTimeout() {
        return feignConnectTimeout;
    }

    public void setFeignConnectTimeout(int feignConnectTimeout) {
        this.feignConnectTimeout = feignConnectTimeout;
    }

    public int getFeignReadTimeout() {
        return feignReadTimeout;
    }

    public void setFeignReadTimeout(int feignReadTimeout) {
        this.feignReadTimeout = feignReadTimeout;
    }
}
