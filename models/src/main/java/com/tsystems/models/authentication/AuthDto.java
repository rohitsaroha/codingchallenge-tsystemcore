package com.tsystems.models.authentication;

import lombok.NonNull;

import javax.validation.constraints.NotBlank;

/**
 * The Auth Dto ideally should be kept as light as possible and should have minimalistic information.
 * But depending on the collective decisions more information
 * like subscription quota information and type of subscriptions can be added here.
 * We can save one time call to client details micro service with this.
 */
public class AuthDto {
    @NotBlank
    private String clientID;
    @NotBlank
    private String subscriptionName;
}
