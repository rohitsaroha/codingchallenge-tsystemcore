package com.tsystems.models.authentication;

import javax.validation.constraints.NotBlank;
/*
Contains Authenticated information required to establish the subscription details
 */
public class AuthEntity {
    @NotBlank
    private String clientID;
    @NotBlank
    private String subscriptionID;
}
