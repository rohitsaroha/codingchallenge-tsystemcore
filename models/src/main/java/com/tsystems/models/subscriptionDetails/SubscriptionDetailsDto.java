package com.tsystems.models.subscriptionDetails;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * The data transfer object to store SubscriptionDetails and exchange information among different services
 * it contains the refrence of notification product id which will help us to map to correct notification product
 * it is helpful in scenarios where multiple email service channels exists to support service above or below a threshold level.
 */
@Data
@NoArgsConstructor
public class SubscriptionDetailsDto {
    private String subscriptionID;
    private String subscriptionName;
    private String clientId;
    private String notificaitonType;
    private String notificationProductId;
    private Date subscriptionStartDate;
    private Date subscriptionEndDate;
    private long subscriptionQuota;

}
