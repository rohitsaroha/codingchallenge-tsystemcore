package com.tsystems.models.subscriptionDetails;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

/**
 * The JPA entity object to store SubscriptionDetails
 */
@Data
@NoArgsConstructor
public class SubscriptionDetails {
    private String subscriptionID;
    private String subscriptionName;
    private String clientId;
    private String planId;
    private Date subscriptionStartDate;
    private Date subscriptionEndDate;
    private long subscriptionQuota;
}
